# TP for ANF SPARQL

Program of ANF SPARQL is [online](https://rbdd.cnrs.fr/spip.php?article355)
From 4 to 6th october 2021 @ Tours, MSHS

It uses Portic data (just an extract) and yarrrml as an example to build and query a triple store.

Récupérer le dépôt en ligne sur le [gitlab d'huma-num](https://gitlab.huma-num.fr/portic/porticttl) car il contient tout : 
- les données dans /csv
- les fichiers de règles dans /yml
- les fichiers RML générés dans /rml
- les triplets générés dans /rml

Ainsi que la source (format Markdown) du [guide](https://gitlab.huma-num.fr/portic/porticttl/-/blob/main/writing_rules_yarrrml.md) aussi publié au format PDF.   


