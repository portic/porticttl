ANF SPARQL 2021 - Modéliser et utiliser YARRRML pour monter ses données dans un triplestore


Auteurs : 
- Christine Plumejeaud-Perreau, UMR 7301 MIGRINTER, 
- Jean-Baptiste Pressac, CRBC, EA 4451 / UMS 3554
Copyright : CC4.0-BY-SA

Pour poursuivre, inscrivez vous sur la [liste de diffusion](https://listes.services.cnrs.fr/wws/info/ateliers-sparql) 

---

SOMMAIRE
- [1. Pré-requis](#1-pré-requis)
- [2. Présentation du modèle](#2-présentation-du-modèle)
- [3. Fabriquer les triplets avec YARRRML](#3-fabriquer-les-triplets-avec-yarrrml)
  - [3.1. Définition d'un port](#31-définition-dun-port)
    - [3.1.1. Première étape : définition d'une source et d'un mapping](#311-première-étape--définition-dune-source-et-dun-mapping)
      - [3.1.1.1. Générer le RML](#3111-générer-le-rml)
      - [3.1.1.2. Générer les triplets](#3112-générer-les-triplets)
      - [3.1.1.3. Pour l'interopérabilité, ajoutons la référence à geonames](#3113-pour-linteropérabilité-ajoutons-la-référence-à-geonames)
    - [3.1.2. étape 2 : Ajouts de propriétés](#312-étape-2--ajouts-de-propriétés)
      - [3.1.2.1. Interrogation SPARQL](#3121-interrogation-sparql)
  - [3.2. Définition d'un point, qui est une géometrie](#32-définition-dun-point-qui-est-une-géometrie)
  - [3.3. Connecter les ports avec les points](#33-connecter-les-ports-avec-les-points)
  - [3.4. Définir ShipDescription](#34-définir-shipdescription)
    - [3.4.1. Exercice : Rajouter le lien entre les descriptions des bateaux et leur port d'attache](#341-exercice--rajouter-le-lien-entre-les-descriptions-des-bateaux-et-leur-port-dattache)
  - [3.5. Définir une escale et sa date d'accostage/appareillage](#35-définir-une-escale-et-sa-date-daccostageappareillage)
    - [3.5.1. Utiliser les fonctions](#351-utiliser-les-fonctions)
    - [3.5.2. Générer les rules en RML et les triplets](#352-générer-les-rules-en-rml-et-les-triplets)
    - [3.5.3. Etendre les fonctions](#353-etendre-les-fonctions)
  - [3.6. Changer de source et utiliser un SGBD](#36-changer-de-source-et-utiliser-un-sgbd)
- [4. Requêtes SPARQL](#4-requêtes-sparql)
  - [4.1. Tonneaux vers mètres cubes](#41-tonneaux-vers-mètres-cubes)
  - [4.2. Tonneaux vers mètres cubes avec QUDT](#42-tonneaux-vers-mètres-cubes-avec-qudt)
- [5. Introduire des unités de mesure intéropérables](#5-introduire-des-unités-de-mesure-intéropérables)
  - [5.1. Quantities, Units, Dimensions, and Types](#51-quantities-units-dimensions-and-types)
  - [5.2. L'exemple du tonnage d'un navire](#52-lexemple-du-tonnage-dun-navire)
    - [5.2.1. Ce que dit l’historienne Silvia Margalli](#521-ce-que-dit-lhistorienne-silvia-margalli)
    - [5.2.2. Bibliographie](#522-bibliographie)
  - [5.3. Utiliser QUDT](#53-utiliser-qudt)
    - [5.3.1. Identifier les unités dans QUDT](#531-identifier-les-unités-dans-qudt)
      - [5.3.1.1. cubic meter](#5311-cubic-meter)
      - [5.3.1.2. cubic feet](#5312-cubic-feet)
    - [5.3.2. Convertir avec QUDT](#532-convertir-avec-qudt)
    - [5.3.3. Retrouvez la longueur d’un pied français](#533-retrouvez-la-longueur-dun-pied-français)
- [6. Vérifier la consistence des données avec SHACL](#6-vérifier-la-consistence-des-données-avec-shacl)
- [7. Suite](#7-suite)


# 1. Pré-requis

Nous avons installé sur notre machine RMLMapper et YARRRML parser

Tout est expliqué dans un [tutoriel en ligne](https://rml.io/yarrrml/tutorial/getting-started/#prerequisites)

L'installation nécessite node.js pour YARRRML parser et un java jdk avec la lib tools.jar pour RMLMapper. Comme ce n'est pas toujours simple à installer, nous vous proposons d'utiliser à la place l'outil [MATEY](https://rml.io/yarrrml/matey/#) en ligne pour fabriquer vos mappings. 
Par ailleurs, le jar de RMLMapper peut-être [téléchargé en ligne](https://github.com/RMLio/rmlmapper-java/releases), et ce sera mieux pour générer un gros volume de données. Matey reste un outil pratique pour générer les règles de transformation RML. 


Toutes les infos utiles sont [en ligne ici](https://rml.io/yarrrml/)

# 2. Présentation du modèle

![Modèle](./fig/STmodel-portic24sept2021.png "Le modèle de Portic")

Il intègre plusieurs ontologies
- [Web Annotation Data model](https://www.w3.org/TR/annotation-model/)
- [GeoSparql](http://www.opengis.net/ont/geosparql#)
- [OwlTime](https://www.w3.org/TR/owl-time/)
- [QUdt](http://www.qudt.org/)
- [Foaf](http://www.foaf-project.org/)

Ce modèle s'est construit progressivement en analysant un corpus attributaire qui au départ met quasiment tout sur le même plan, dans un fichier d'escales - les *Pointcalls*. Ces données décrivent les déplacements de navires pilotés par des capitaines qui s'arrêtent et font escale dans certains ports de France et du monde pour charger et décharger des cargaisons, et "accessoirement" payer les taxes afférentes. La source de ces informations est en majeure partie celles de registres fiscaux de l'Amiral de France, qui percevait une taxe sur tout départ de navire depuis un port de ses juridictions (que sont les amirautés du Royaume de France, 1787). 

Il s'inspire aussi de la conception par patrons, et particulier de **trajectoires**, [telles que décrite ici](http://ontologydesignpatterns.org/wiki/Submissions:Trajectory).
On retrouve dans ce catalogue la mention de [QUdt](http://ontologydesignpatterns.org/wiki/Ontology:QUDT:_Quantities%2C_Units%2C_Dimensions_and_Types) par exemple pour préciser **les unités de mesure**. 
Egalement la description d'**événement** - [description](http://ontologydesignpatterns.org/wiki/Submissions:EventCore) -, si on veut considérer que l'escale d'un navire dans un port à une certaine date est un événement.

Les **annotations** sont utilisées pour marquer un travail d'identification d'objets, navires ou capitaines, réalisé par un agent, en l'occurence [Silvia Marzagalli](http://orcid.org/0000-0003-0387-470X/), historienne. A partir d'un ensemble de déclarations de départs ou d'arrivée de navires pilotés par des capitaines et dont les orthographes varient d'un port à l'autre, d'une date à l'autre, elle "reconnait" en utilisant un ensemble d'heuristiques les même objets, pour leur attribuer un identifiant sur 8 digits. Nous considérons dans l'ontologie que l'identifiant est une annotation d'un navire réalisée par un agent, l'experte en Histoire, et cet identifiant permet d'imaginer la trajectoire d'un navire sur plusieurs ports à différentes dates, et la trajectoire d'un capitaine qui ne pilote pas toujours le même navire. L'agent qui annote est modélisé comme une **personne** avec FOAF.

Les escales sont réalisées dans des ports qui ont une **localisation** qui est modélisée par un point en GeoSparql. 
Les escales se font à un certain moment, (on sait si c'est exactement, après ou avant une date), qui se modélise par **un instant** avec OwlTime.

Pour ce TP, nous allons grandement simplifier ce modèle et nous concentrer sur un sous-ensemble de classes. 
![Modèle](./fig//tp_portic_24sept2021.png "Le modèle du TP de Portic")


# 3. Fabriquer les triplets avec YARRRML

Tutoriel inspiré et adapté du site de ressources [RML](https://rml.io/)

Récupérer le dépôt en ligne sur le [gitlab d'huma-num](https://gitlab.huma-num.fr/portic/porticttl) car il contient tout : 
- les données dans /csv
- les fichiers de règles dans /yml
- les fichiers RML générés dans /rml
- les triplets générés dans /rml

Ainsi que la source (format Markdown) de ce fichier.

Note : nous cherchons une extension pour faire reconnaître la syntaxe sparql à Markdown.
- https://support.codebasehq.com/articles/tips-tricks/syntax-highlighting-in-markdown 
- https://github.com/mjbvz/vscode-fenced-code-block-grammar-injection-example


## 3.1. Définition d'un port

### 3.1.1. Première étape : définition d'une source et d'un mapping

On voudrait fabriquer ces triplets
```yml
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix dataportic: <http://data.portic.fr/>.
@prefix ontoportic: <http://model.portic.fr/>.

dataportic:A0122594 a ontoportic:Port.
dataportic:A0135548 a ontoportic:Port.
dataportic:A0122883 a ontoportic:Port.

```

Se connecter à une source, définir votre sujet.

Nous écrivons `rules_ports_v1.yml`

```yml
prefixes:
  dataportic: "http://data.portic.fr/"
  ontoportic: "http://model.portic.fr/"

mappings:
  ports:
    sources:
      - access: ports.csv
        referenceFormulation: csv
    s: dataportic:$(uhgs_id)
    po:
      - [a, ontoportic:Port]
```


#### 3.1.1.1. Générer le RML

Comment ? En ligne avec Matey : https://rml.io/yarrrml/matey/#

yarrrml-parser -i yml/rules_ports_v1.yml -o rml/rules_ports_v1.rml.ttl

```ttl
@prefix rr: <http://www.w3.org/ns/r2rml#>.
@prefix rml: <http://semweb.mmlab.be/ns/rml#>.
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
@prefix ql: <http://semweb.mmlab.be/ns/ql#>.
@prefix map: <http://mapping.example.com/>.

map:map_ports_000 rml:logicalSource map:source_000;
    a rr:TriplesMap;
    rdfs:label "ports";
    rr:subjectMap map:s_000;
    rr:predicateObjectMap map:pom_000.
map:om_000 a rr:ObjectMap;
    rr:constant "http://model.portic.fr/Port";
    rr:termType rr:IRI.
map:pm_000 a rr:PredicateMap;
    rr:constant rdf:type.
map:pom_000 a rr:PredicateObjectMap;
    rr:predicateMap map:pm_000;
    rr:objectMap map:om_000.
map:rules_000 a <http://rdfs.org/ns/void#Dataset>;
    <http://rdfs.org/ns/void#exampleResource> map:map_ports_000.
map:s_000 a rr:SubjectMap;
    rr:template "http://data.portic.fr/{uhgs_id}".
map:source_000 a rml:LogicalSource;
    rml:source "ports.csv";
    rml:referenceFormulation ql:CSV.
```

#### 3.1.1.2. Générer les triplets

Comment ? 

Depuis le répertoire où se trouve le fichier ports.csv et le fichier rules_ports.rml.ttl (en supposant que [rmlmapper.jar](https://github.com/RMLio/rmlmapper-java/releases) a été copié dans le répertoire C:\Tools\rmlmapper-java-master), executer :

java -jar C:\Tools\rmlmapper-java-master\rmlmapper.jar -m rml/rules_ports_v1.rml.ttl -o ttl/ports_v1.ttl

#### 3.1.1.3. Pour l'interopérabilité, ajoutons la référence à geonames

A partir de maintenant, nous éditons le fichier `rules_ports_v2.yml`

(Exercice)
geonameid --> owl:sameAs  gn:geonameid

Quel préfix ?
http://www.geonames.org/ontology#
https://sws.geonames.org/

https://sws.geonames.org/2979309
--> https://www.geonames.org/2979309/saint-jean-de-luz.html

yarrrml-parser -i yml/rules_ports_v2.yml -o rml/rules_ports_v2.rml.ttl

java -jar C:\Tools\rmlmapper-java-master\rmlmapper.jar -m rml/rules_ports_v2.rml.ttl -o ttl/ports_v2.ttl

**Quelle propriété ? Est-ce que owl:sameAs est vraiment satisfaisant ?**

A partir de maintenant, nous éditons le fichier `rules_ports_v3.yml`

Un port quelque part en Norvège (identifié A0840357) serait équivalent au pays lui-même ?
A la place de ce prédicat, je propose d'utiliser quelque chose de moins risqué : généralement, les ports se développent aux alentours de villes (et vice-versa). Donc nous pourrions utiliser le prédicat spatial : "à l'intérieur de" sans trop mentir.

Ce prédicat est défini dans l'ontologie GeoSparql.
Quelques [ressources en ligne](http://www.lirmm.fr/rod/slidesRoD04102018/RoD2018-tutorial.pdf) vous expliquent mieux.

Prefix à utiliser : ogc: http://www.opengis.net/ont/geosparql#
Prédicat : ogc:sfWithin

yarrrml-parser -i yml/rules_ports_v3.yml -o rml/rules_ports_v3.rml.ttl

java -jar C:\Tools\rmlmapper-java-master\rmlmapper.jar -m rml/rules_ports_v3.rml.ttl -o ttl/ports_v3.ttl

Denière remarque (merci Hervé) : un point à l'intérieur d'un autre point, c'est topologiquement impossible. Essayons donc plutôt `ogc:sfEquals` pour faire mieux.
### 3.1.2. étape 2 : Ajouts de propriétés

Rajouter quelques propriétés descriptives des ports.

A partir de maintenant, nous éditons le fichier `rules_ports_v4.yml`


```yml
prefixes:
  dataportic: "http://data.portic.fr/"
  ontoportic: "http://model.portic.fr/"

mappings:
  port:
    sources:
      - access: ports.csv
        referenceFormulation: csv
    s: dataportic:$(uhgs_id)
    po:
      - [a, ontoportic:Port]
      - [ontoportic:toponyme_fr, $(toponyme_standard_fr), fr~lang]
      - [ontoportic:shippingArea, $(shiparea)]
      - [ontoportic:amiraute, $(amiraute)]
```

Dans cet exemple, on spécifie la langue utilisée (le français). Les données sont aussi disponibles en anglais, mais nous avons sélectionné l'attribut standardisé en français. 

#### 3.1.2.1. Interrogation SPARQL

Importer les triplets dans GraphDB avec pour paramètres d'importation (Import settings) : 
1. *Base IRI* : laisser vide
2. *Target graph* : cocher *From data*.

Puis aller sur l'interrogation
http://localhost:7200/sparql

Retrouver le port qui s'appelle Saint-Jean-de-Luz.

```rq
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX portic: <http://model.portic.fr/>

select * where { 
    ?s portic:toponyme_fr "Saint-Jean-de-Luz"@fr.
}
```

Ok renvoie le port (l'entité complète)

Maintenant, je veux la liste des ports (par leur nom) des ports situés sur le golfe de gascogne, plus particulièrement la mer des pertuis (shippingarea = ACE-ROCH)

```rq
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX portic: <http://model.portic.fr/>

select ?nom where { 
    ?s portic:shippingarea "ACE-ROCH";
       portic:toponyme_fr ?nom
} limit 100 
```

La réponse contient 6 items dans notre démonstration: Saint-André-de-Cubzac, Royan, Châlon, l'Houmée, Are-en-Ré, La Flotte-en-Ré, Ribérou.


## 3.2. Définition d'un point, qui est une géometrie

On voudrait fabriquer ces triplets
```yml
@prefix dataportic: http://data.portic.fr/point/ .
@prefix rdf: http://www.w3.org/1999/02/22-rdf-syntax-ns# .
@prefix ogc: http://www.opengis.net/ont/geosparql# .

dataportic:A0840357 rdf:type ogc:Geometry.
dataportic:A0840357 ogc:asWKT "POINT( 10  62 )"^^ogc:wktLiteral.
```

```yml
prefixes:
  dataportic: "http://data.portic.fr/"
  ontoportic: "http://model.portic.fr/"
  ogc: "http://www.opengis.net/ont/geosparql#"

mappings:
    point:
        sources:
          - access: ports.csv
            referenceFormulation: csv
        s: dataportic:point/$(uhgs_id)
        po:
          - [a, geosparl:Geometry]
          - [ogc:asWKT, POINT( $(longitude)  $(latitude) ), ogc:wktLiteral]
```

## 3.3. Connecter les ports avec les points

Remarque : l'usage de graphes, qui est introduit ici, est optionnel. 

Et ainsi on génère des quads et non des triplets. **Pour que graphDB les accepte, il faut renommer le fichier de triplets turtle (.ttl) en N-Quads (.nq). Il suffit de changer l'extension à la main.**
Voir wikidata : https://en.wikipedia.org/wiki/N-Triples#N-Quads

```yml
prefixes:
  dataportic: "http://data.portic.fr/"
  ontoportic: "http://model.portic.fr/"
  ogc: "http://www.opengis.net/ont/geosparql#"

mappings:
  port:
    sources:
      - access: ports.csv
        referenceFormulation: csv
    s: dataportic:port/$(uhgs_id)
    graphs: ontoportic:Ports
    po:
      - [a, ontoportic:Port]
      - [ontoportic:toponyme_fr, $(toponyme_standard_fr), fr~lang]
      - [ontoportic:shippingArea, $(shiparea)]
      - [ontoportic:amiraute, $(amiraute)]
      - [ontoportic:province, $(province)]
      - p: ontoportic:state_1789_fr
        o:
         value: $(state_1789_fr)
         language: fr
      - p: ogc:hasGeometry
        o:
          mapping: point
          condition:
            function: equal
            parameters:
              - [str1, $(uhgs_id), s]
              - [str2, $(uhgs_id), o]
        
  point:
    sources:
      - access: ports.csv
        referenceFormulation: csv
    s: dataportic:point/$(uhgs_id)
    graphs: ontoportic:Points
    po:
      - [a, ogc:Geometry]
      - [ogc:asWKT, POINT( $(longitude)  $(latitude) ), ogc:wktLiteral]
```          

Générer les rules et les triplets correspondants : 

yarrrml-parser -i rules_ports.yml -o rules_ports.rml.ttl

java -jar C:\Tools\rmlmapper-java-master\rmlmapper.jar -m rules_ports.rml.ttl -o ports.ttl

## 3.4. Définir ShipDescription

Les descriptions de navire sont dans un autre fichier, pointcalls.csv, qui a comme séparateur ';' (le point-virgule).
Les drapeaux (flag) sont spécifiés en anglais et en français. On pourrait aussi exporter le flag en anglais.

```yaml

prefixes:
  dataportic: "http://data.portic.fr/"
  ontoportic: "http://model.portic.fr/"
  geosparl: "http://www.opengis.net/ont/geosparql#"

mappings:
  shipdescription:
    sources:
      - access: pointcall.csv
        referenceFormulation: csv
        delimiter: ';'
    s: dataportic:shipdescription/$(source_doc_id)_$(ship_id)
    #Tester dataportic:shipdescription/$(record_id)
    po:
      - [a, ontoportic:ShipDescription]
      - [ontoportic:tonnage, $(tonnage)]
      - [ontoportic:tonnage_unit, $(tonnage_unit)]
      - [ontoportic:name, $(ship_name)]
      - [ontoportic:ship_class, $(class)]
      - p: ontoportic:flag
        o:
         value: $(ship_flag_standardized_fr)
         language: fr
      
  
```

On peut discuter de l'unicité de la ShipDescription. En effet, $(source_doc_id)_$(ship_id) fournit une description unique de navire par document source. Mais l'outil de mapping n'utilise pas IRI (unique) pour dédoublonner la sortie. En conséquence, on peut mettre (record_id) comme identifiant de ShipDescription sans impact. 

yarrrml-parser -i rules_pointcalls.yml -o rules_pointcalls.rml.ttl

java -jar C:\Tools\rmlmapper-java-master\rmlmapper.jar -m rules_pointcalls.rml.ttl -o pointcalls.ttl

### 3.4.1. Exercice : Rajouter le lien entre les descriptions des bateaux et leur port d'attache

Liez la description d'un navire (ShipDescription) au port d'attache du navire (Port) par la propriété *ontoportic:hasHomeport*. La correspondance entre un ShipDescription et le port d'attache est décrite dans le fichier pointcall.csv (caractère de séparation : le point-virgule) : la colonne homeport_uhgs_id contient la référence au port.


yarrrml-parser -i yml/rules_pointcalls.yml -o rml/rules_pointcalls.rml.ttl

java -jar C:\Tools\rmlmapper-java-master\rmlmapper.jar -m rml/rules_pointcalls.rml.ttl -o ttl/pointcalls.nq

Solution

```yml
# Rajouter ces lignes au mapping des ports
      - p: ontoportic:hasHomeport
        o:
          mapping: port
          condition:
            function: equal
            parameters:
              - [str1, $(homeport_uhgs_id), s]
              - [str2, $(uhgs_id), o]
        #graphs: ontoportic:Ports
```

Le fichier complet

```yml
# Le fichier YARRRML final :
prefixes:
  dataportic: "http://data.portic.fr/"
  ontoportic: "http://model.portic.fr/"
  time: "http://www.w3.org/2006/time#"
  grel: "http://users.ugent.be/~bjdmeest/function/grel.ttl#"
  #grel_local: "file:///Travail/CNRS_mycore/DISA/RBDD/ANF2021/PORTIC_modele/ports/grel_local.ttl#"
  fnct_added: "file:///Travail/CNRS_mycore/DISA/RBDD/ANF2021/PORTIC_modele/ports/functions_subset.ttl#" 
  xsd: "https://www.w3.org/TR/xmlschema11-2#"
  gn: "https://sws.geonames.org/"
  ogc: "http://www.opengis.net/ont/geosparql#"

mappings:
  port:
    sources:
      - access: csv/ports.csv
        referenceFormulation: csv
    s: dataportic:port/$(uhgs_id)
    graph: ontoportic:Ports
    po:
      - [a, ontoportic:Port]
      - [ogc:sfEquals, gn:$(geonameid)~iri]
      - [ontoportic:toponyme_fr, $(toponyme_standard_fr)]
      - [ontoportic:shippingarea, $(shiparea)]
      - [ontoportic:amiraute, $(amiraute)]
      - [ontoportic:province, $(province)]
      - p: ontoportic:etat
        o:
         value: $(state_1789_fr)
         language: fr
      - p: ogc:hasGeometry
        o:
          mapping: point
          condition:
            function: equal
            parameters:
              - [str1, $(uhgs_id), s]
              - [str2, $(uhgs_id), o]
        
        
  point:
    sources:
      - access: csv/ports.csv
        referenceFormulation: csv
    s: dataportic:point/$(uhgs_id)
    graph: ontoportic:Ports
    po:
      - [a, ogc:Geometry]
      - [ogc:asWKT, POINT( $(longitude)  $(latitude) ), ogc:wktLiteral]

  shipdescription:
    sources:
      - access: csv/pointcall.csv
        referenceFormulation: csv
        delimiter: ';'
    s: dataportic:shipdescription/$(source_doc_id)_$(ship_id)
    graph: ontoportic:Pointcalls
    po:
      - [a, ontoportic:ShipDescription]
      - [ontoportic:tonnage, $(tonnage)]
      - [ontoportic:tonnage_unit, $(tonnage_unit)]
      - [ontoportic:name, $(ship_name)]
      - [ontoportic:ship_class, $(class)]
      - p: ontoportic:flag
        o:
         value: $(ship_flag_standardized_fr)
         language: fr
      - p: ontoportic:hasHomeport
        o:
          mapping: port
          condition:
            function: equal
            parameters:
              - [str1, $(homeport_uhgs_id), s]
              - [str2, $(uhgs_id), o]
        #graphs: ontoportic:Ports
  
  pointcall:
    sources:
      - access: csv/pointcall.csv
        referenceFormulation: csv
        delimiter: ';'
    s: dataportic:pointcall/$(record_id)
    graph: ontoportic:Pointcalls
    po:
      - [a, ontoportic:Pointcall]
      - [ontoportic:status, $(navigo_status)]
      - [ontoportic:action, $(pointcall_action)]
      - [ontoportic:function, $(pointcall_function)]
      - p: ontoportic:hasShipDescription
        o:
          mapping: shipdescription
          condition:
            function: equal
            parameters:
              - [str1, $(record_id), s]
              - [str2, $(record_id), o]
        #graphs: ontoportic:Pointcalls
      - p: ontoportic:hasPlace
        o:
          mapping: port
          condition:
            function: equal
            parameters:
              - [str1, $(pointcall_uhgs_id), s]
              - [str2, $(uhgs_id), o]
        #graphs: ontoportic:Ports             
```


1. Vérifier si vos descriptions pointent vers un homeport
   Pourquoi ?

2. Faire le TP sans ces 2 lignes dans ports.csv 

50.1,1.833333,Abbeville,Abbeville,A0169591, MAN-WIGH,France,,France,,Picardie,Abbeville,true,60,33,,,siège amirauté,Amiens,Abbeville,0,,France,,0,Abbeville,3038789
47.566667,-2.95,Locmariaquer,Locmariaquer,A0186503,ACE-IROI,France,,France,,Bretagne,Vannes,true,,,,,oblique,Lorient,Vannes,0,,France,,0,Locmariaquer,2997964

3. Rajouter ces deux lignes dans ports.csv
   
Vérifier le fichier pointcalls.nq : est-ce que tous les pointcalls ont un homeport ?
Discussion

## 3.5. Définir une escale et sa date d'accostage/appareillage

Nous allons utiliser owlTime pour dater la date d'arrivée ou de départ du navire. 

Owltime
https://www.w3.org/TR/owl-time/
http://www.w3.org/2006/time#

Cela pourrait être aussi simple que `time:hasTime, $(date_fixed), xsd:date` avec l'import de time `http://www.w3.org/2006/time#` 
Mais comme la date d'arrivée ou de départ des navires n'est pas connue systématiquement d'après nos sources historiques, car un navire : 
- soit déclare qu'il part d'un port départ vers un port arrivée à la date x, mais alors la date d'arrivée y n'est pas connue. On sait juste qu'elle sera après x. Dans ce cas, les historiens renseignent le champs `pointcall_out_date` avec une convention spéciale. C'est le cas majoritaire des sources fiscales de l'ancien Régime (Congés, série G5)
- soit déclare qu'il arrive à tel port arrivée à la date x, et il repartira vers d'autre ports après x, sans connaitre là non plus ces dates. Dans ce cas, les historiens renseignent le champs `pointcall_in_date` avec la même convention spéciale. C'est le cas majoritaire des sources de Marseille (Registre de la Santé)

Cette convention spéciale permet de trier les dates par ordre lexicographique, mais ne correspond pas au format d'écriture ISO d'un champs date qui est dd-MM-YYYY (06-10-2021 pour le 6 octobre 2021). Ils utilisent les signe '=', '<', '>' respectivement pour donner la date et la précision relative : 
  -  '=' pour exactement à la date
  - '<' pour avant cette date
  - '>' pour après cette date

Nous allons exploiter cette convention pour ajouter un champs `date_position` qui dira si la date indiquée est exactement, avant ou après l'instant (au sens OwlTime) qui sera spécifié.

Pour cela, nous allons mobiliser les fonctions de Grel pour analyser la chaîne de charactère pointcall_out_date et vérifier si elle contient plutôt des caractères '<' ou des '>'. 

Ensuite nous dirons que les Pointcall ont une date grâce au prédicat **hasDatatation** qui lui même précisera la date avec **hasTime**
https://www.w3.org/TR/owl-time/#time:hasTime 

Idéalement, il faudrait déclarer notre Datation comme spécialisation d'une TemporalEntity, qui se termine ou commence à telle date
https://www.w3.org/TR/owl-time/#time:TemporalEntity

    - time:hasBeginning si on connait la date de début d'escale (pointcall_in_date)
    https://www.w3.org/TR/owl-time/#time:hasBeginning

    - time:hasEnd si on connait la date de fin d'escale (pointcall_out_date)
    https://www.w3.org/TR/owl-time/#time:hasEnd

https://www.w3.org/TR/owl-time/#time:Instant
https://www.w3.org/TR/owl-time/#time:inXSDDate
ex:date "1995-09-18"^^xsd:date .

Cependant, cela signifie que la propriété qui relie Pointcall à Datation est calculée, mais je n'ai pas réussi à faire calculer la valeur de cette propriété avec les fonctions, alors que j'ai réussi avec le calcul des valeurs d'objets.


### 3.5.1. Utiliser les fonctions

L'exemple du tutorial
```
p: dbo:hairColor
o:
  function: grel:toUpperCase
  parameters:
    - [grel:valueParameter, $(hair color)]
  language: en

```


Comme dans l'exemple : passer le nom des amirautés en majuscules
```
      - p: ontoportic:bidon
        o:
          function: grel:toUpperCase
          parameters:
            - [grel:valueParameter, $(pointcall_admiralty)]
```

Vérifier la position du caractère '<' dans une chaine de caractères. Si non présent, c'est -1
Passer 2 paramètres dans une fonction
```
      - p: ontoportic:test
        o:
          function: grel:string_lastIndexOf(valueParameter = toto2, string_sub=>)
      - p: ontoportic:test2
        o: 
          function: grel:controls_if
          parameters:
            - [grel:bool_b, true]
            - [grel:any_true, apres]
            - [grel:any_false, sinon]          
      - p: ontoportic:apres
        o: 
          function: grel:string_contains(valueParameter = $(pointcall_in_date),string_sub = >)
      - p: ontoportic:avant
        o: 
          function: grel:string_contains(valueParameter = $(pointcall_out_date),string_sub = <)
```

Enchainer les fonctions pour calculer une valeur
```
      - p: ontoportic:date_position
        o:
          function: grel:controls_if
          parameters:
            - parameter: grel:bool_b
              value:
                function: grel:string_contains
                parameters:
                  - [grel:valueParameter, $(pointcall_in_date)]
                  - [grel:string_sub, >]
            - [grel:any_true, ontoportic:after]
            - parameter: grel:any_false
              value:
                function: grel:controls_if
                parameters:
                  - parameter: grel:bool_b
                    value:
                      function: grel:string_contains
                      parameters:
                        - [grel:valueParameter, $(pointcall_out_date)]
                        - [grel:string_sub, <]
                  - [grel:any_true, ontoportic:before]
                  - [grel:any_false, ontoportic:exact]
```

### 3.5.2. Générer les rules en RML et les triplets

```yml
prefixes:
  dataportic: "http://data.portic.fr/"
  ontoportic: "http://model.portic.fr/"
  time: "http://www.w3.org/2006/time#"
  grel: "http://users.ugent.be/~bjdmeest/function/grel.ttl#"
  xsd: "https://www.w3.org/TR/xmlschema11-2#"
  gn: "https://sws.geonames.org/"
  ogc: "http://www.opengis.net/ont/geosparql#"

# Rajout du mapping
  datation:
    sources:
      - access: csv/pointcall.csv
        referenceFormulation: csv
        delimiter: ';'
    s: dataportic:datation/$(record_id)
    graph: ontoportic:Pointcalls
    po:
      - [a, ontoportic:Datation]
      - [ontoportic:outdate, $(pointcall_out_date)]
      - [ontoportic:indate, $(pointcall_in_date)]
      - [time:hasTime, $(date_fixed), xsd:date]
      - p: ontoportic:date_position
        o:
          function: grel:controls_if
          parameters:
            - parameter: grel:bool_b
              value:
                function: grel:string_contains
                parameters:
                  - [grel:valueParameter, $(pointcall_in_date)]
                  - [grel:string_sub, >]
            - [grel:any_true, ontoportic:after]
            - parameter: grel:any_false
              value:
                function: grel:controls_if
                parameters:
                  - parameter: grel:bool_b
                    value:
                      function: grel:string_contains
                      parameters:
                        - [grel:valueParameter, $(pointcall_out_date)]
                        - [grel:string_sub, <]
                  - [grel:any_true, ontoportic:before]
                  - [grel:any_false, ontoportic:exact]
```

```yml
## Rajout du lien avec les pointcalls dans le mapping pointcall

      - p: ontoportic:hasRelativeDatation
        o:
          mapping: datation
          condition:
            function: equal
            parameters:
              - [str1, $(record_id), s]
              - [str2, $(record_id), o]
      
```

`yarrrml-parser -i yml/rules_time.yml -o rml/rules_time.rml.ttl`

`java -jar C:\Tools\rmlmapper-java-master\rmlmapper.jar -m rml/rules_time.rml.ttl -o ttl/portic.nq`

Une fois que vous avez terminé, et importé dans un triplestore comme GraphDB par exemple, voici le résultat
![Modèle](./fig/pointcall_00104908.PNG "Le modèle du TP de Portic")




### 3.5.3. Etendre les fonctions 

Ce n'est pas un franc succès. Je dois d'abord faire remarquer que les fonctions qui sont incluses dans le jar de RMLMapper.jar ne sont pas identiques à celles décrites sur http://users.ugent.be/~bjdmeest/function/grel.ttl#
Et par exemple, la fonction math_max qui devrait parfaitement marcher avec cet appel, et bien ne marche pas tout simplement parce qu'elle est commentée dans le fichier grel.ttl inclus dans le jar. 

    `function: grel:math_max(p_dec_n = 1, param_n2 = 2)`

Par ailleurs, il faut bien lire la description des fonctions et passer en paramètre le prédicat correspondant à un paramètre et pas directement le nom du paramètre. 

par exemple : 
```
    function: grel:toUpperCase(grel:valueParam = $(pointcall_admiralty))
```
Ne marche pas car il faut écrire `grel:valueParameter` à la place de `grel:valueParam`

Et ce n'est pas si évident si on lit la description de la fonction : 
```
grel:prob_ucase
    a                   fno:Problem ;
    fno:name            "The ucase problem"^^xsd:string ;
    dcterms:description "Converting a string to upper case characters."^^xsd:string .

grel:toUpperCase
    a                   fno:Function ;
    fno:name            "to Uppercase" ;
    rdfs:label          "to Uppercase" ;
    dcterms:description "Returns the input with all letters in upper case." ;
    fno:solves          grel:prob_ucase ;
    fno:expects         ( grel:valueParam ) ;
    fno:returns         ( grel:stringOut ) .
```

Il faut bien lire la définition de  `grel:valueParam` et appeler la fonction avec le nom avec ce prédicat. 
```
grel:valueParam
    a             fno:Parameter ;
    fno:name      "input value" ;
    rdfs:label    "input value" ;
    fno:predicate grel:valueParameter ;
    fno:type      xsd:string ;
    fno:required  "true"^^xsd:boolean .
```

Ensuite j'ai essayé d'étendre les fonctions utilisées, définir mon propre fichier. En suivant les indications en ligne : 

- https://github.com/RMLio/rmlmapper-java
- https://fno.io/spec/
- https://stackoverflow.com/questions/66414992/rml-and-fno-fails-to-run-together
- Telecharger les functions : https://github.com/RMLio/rmlmapper-java/issues/26
- Source des functions : https://github.com/RMLio/rmlmapper-java/blob/master/src/main/java/be/ugent/rml/functions/lib/GrelProcessor.java

- https://freesoft.dev/program/138707242
>GrelFunctions.jar
-f,--functionfile <arg>          path to functions.ttl file (dynamic functions are found relative to functions.ttl)

> Dynamic loading
Just put the java or jar-file in the resources folder, at the root folder of the jar-location, or the parent folder of the jar-location, it will be found dynamically.
Note: the java or jar-files are found relative to the loaded functions.ttl. You can change the functions.ttl path using a commandline-option (-f).

Essai avec grel_local:
`java -jar C:\Tools\rmlmapper-java-master\rmlmapper.jar -m rules_time.rml.ttl -o time.ttl -f grel.ttl`

```
09:57:50.683 [main] ERROR be.ugent.rml.cli.Main               .main(367) - No mapping was found for the function with IRI <file:///Travail/CNRS_mycore/DISA/RBDD/ANF2021/PORTIC_modele/ports/grel.ttl#toUpperCase> in the function descriptions.
```

- https://www.w3.org/TR/turtle/#sec-iri

Bref, sans succès...

## 3.6. Changer de source et utiliser un SGBD

Connection à Postgres, avec la requêtes suivante pour récupérer les Ports et les points :
```sql
          select latitude,longitude,toponyme_standard_fr,toponyme_standard_en,uhgs_id,shiparea,state_1789_fr,substate_1789_fr,state_1789_en,substate_1789_en,province,amiraute,status,toponyme,geonameid
          from ports.port_points
``` 

Dans l'exemple, le SGBD est sur localhost, il écoute sur le port par défaut, 5432, avec comme login `postgres` et mot de passe `postgres`

```yml
  port:
    sources:
      - access: http://localhost/portic_v6
        type: postgresql
        credentials:
          username: postgres
          password: postgres
        queryFormulation: sql2008
        query: |
          select latitude,longitude,toponyme_standard_fr,toponyme_standard_en,uhgs_id,shiparea,state_1789_fr,substate_1789_fr,state_1789_en,substate_1789_en,province,amiraute,status,toponyme,geonameid
          from ports.port_points
        referenceFormulation: csv
```

Note : comment sont gérées les valeurs nulles ?
Pour assurer un comportement identiques des fonctions, nous utilisons `coalesce(champs, valeur imposée si champs nul)` de postgres. 

```yml
  pointcall:
    sources:
      - access: http://localhost/portic_v6
        type: postgresql
        credentials:
          username: postgres
          password: postgres
        queryFormulation: sql2008
        query: |
          select record_id, pointcall_uhgs_id, ship_id, tonnage,tonnage_unit,ship_name,class,ship_flag_standardized_fr,homeport_uhgs_id,
          navigo_status,pointcall_action,pointcall_function,pointcall_in_date,pointcall_out_date,date_fixed, source_doc_id, source_suite
          from navigoviz.pointcall 
        referenceFormulation: csv
        #encoding: iso8859-1
```

Note : l'encodage de ces données dans Postgres est UTF-8, et normalement RMLMapper suppose un encodage `utf-8` par défaut. Mais il me semble qu'un bug persiste dans RMLMapper (voir une discussion en ligne) qui a été résolu pour les sources de type CSV mais pas pour celles utilisant des connecteurs sur base de données. Du coup c'est le bazard dans les accents du fichiers de triplets générés. 

Après la génération du fichier complet, 2H sur un PC classique, et 300 Mo environ,
on peut uploader dans GraphDB mais avec une option spéciale expliquée ici : 
https://graphdb.ontotext.com/documentation/standard/loading-data-using-the-workbench.html#importing-server-files
Par ailleurs, avant import dans graphDB, il est possible de zipper le fichier de triplets/quads, ce qui réduit considérablement le volume du fichier, et graphDB l'accepte.

>By default, it is $user.home/graphdb-import/.
If you want to tweak the directory location, see the graphdb.workbench.importDirectory system property. The directory is scanned recursively and all files with a semantic MIME type are visible in the Server files tab.

Sur Windows, créer le répertoire graphdb-import et déposer dedans votre fichier .nq ou .ttl ou .zip à importer.


# 4. Requêtes SPARQL

http://localhost:7200/sparql

Par exemple, tester la diversité des unités de mesure (tonnage_unit) avec DISTINCT

```ttl
prefix ontoportic:<http://model.portic.fr/>
select DISTINCT ?unit where {
    ?s ontoportic:tonnage ?tonnage ; 
	ontoportic:tonnage_unit ?unit .
}

```

## 4.1. Tonneaux vers mètres cubes

Ecrivez une requête SPARQL pour afficher le tonnage en mètres cubes en partant du fait que 1 tonneau = 42 * 0.028316846592 mètres cubes.

Le tonnage (`ontoportic:tonnage`) est de type `xsd:string`. Il faut le convertir en nombre à virgule pour que l'opération de multiplication affiche un résultat. La fonction [IF](https://www.w3.org/TR/sparql11-query/#func-if) permet d'appliquer la conversion uniquement aux tonnages exprimés en tonneaux (Portic contient aussi des tonnages exprimés en quintaux). 

```ttl
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
prefix ontoportic: <http://model.portic.fr/>

SELECT * WHERE {
    ?s ontoportic:tonnage ?tonnage ; ontoportic:tonnage_unit ?unit .
    BIND (IF(?unit = 'tx', xsd:float(?tonnage) * 42 *  0.028316846592 , ?tonnage) AS ?tonnage_in_m3)
} 
```

On obtient le même résultat avec un FILTER :

```ttl
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
prefix ontoportic: <http://model.portic.fr/>

SELECT * WHERE {
    ?s ontoportic:tonnage ?tonnage ; ontoportic:tonnage_unit ?unit .
    FILTER (?unit = 'tx')
    BIND (xsd:float(?tonnage) * 42 *  0.028316846592  AS ?tonnage_in_m3)
} 
```
Et si des quintaux à Marseille

```ttl
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
prefix ontoportic: <http://model.portic.fr/>

SELECT * WHERE {
    ?s ontoportic:tonnage ?tonnage ; ontoportic:tonnage_unit ?unit .
    BIND (
      IF(?unit = 'tx', 
        xsd:float(?tonnage) * 42 *  0.028316846592 ,
        IF(?unit = 'quintaux', xsd:float(?tonnage) * 42 / 24.0 *  0.028316846592 , ?tonnage) 
      ) 
      AS ?tonnage_in_m3)
} 
```

## 4.2. Tonneaux vers mètres cubes avec QUDT

Une autre solution possible est d'importer dans GraphDB l'ontologie Unit de [QUDT](http://www.qudt.org) (voir chapitre #5) pour récupérer le taux de convertion des pieds cubes en mètres cubes. En effet, un tonneau = 42 pieds cube (cubic feet). Or, la classe unit:FT3 (reproduite ci-dessous) correspond au cubic foot dans l'ontologie Unit de QUDT et sa propriété qudt:conversionMultiplier correspond à ce fameux taux de conversion. 

A partir de ces informations, réécrivez la requête SPARQL pour afficher le tonnage en mètres cubes.

```ttl
unit:FT3
  a qudt:DerivedUnit ;
  a qudt:Unit ;
  dcterms:description "The cubic foot is an Imperial and US customary unit of volume, used in the United States and the United Kingdom. It is defined as the volume of a cube with sides of one foot (0.3048 m) in length. To calculate cubic feet multiply length X width X height. "^^rdf:HTML ;
  qudt:conversionMultiplier 0.028316846592 ;
  qudt:definedUnitOfSystem sou:IMPERIAL ;
  qudt:definedUnitOfSystem sou:USCS ;
  qudt:expression "\\(ft^{3}\\)"^^qudt:LatexString ;
  qudt:hasDimensionVector qkdv:A0E0L3I0M0H0T0D0 ;
  qudt:hasQuantityKind quantitykind:Volume ;
  qudt:iec61360Code "0112/2///62720#UAA456" ;
  qudt:ucumCode "[cft_i]"^^qudt:UCUMcs ;
  qudt:ucumCode "[ft_i]3"^^qudt:UCUMcs ;
  qudt:uneceCommonCode "FTQ" ;
  qudt:unitOfSystem sou:IMPERIAL ;
  rdfs:isDefinedBy <http://qudt.org/2.1/vocab/unit> ;
  rdfs:label "Cubic Foot"@en ;
.
```


Grace à l'ontologie unit de QUDT (à importer dans GraphDB dans le dépot des triplets de Portic), on récupère le taux de conversion des pieds cubes vers les mètres cubes.

```ttl
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX unit: <http://qudt.org/vocab/unit/>
PREFIX qudt: <http://qudt.org/schema/qudt/>
prefix ontoportic: <http://model.portic.fr/>

SELECT * WHERE {
    ?s ontoportic:tonnage ?tonnage ; ontoportic:tonnage_unit ?unit .
    unit:FT3 qudt:conversionMultiplier ?taux .
    BIND (IF(?unit = 'tx', xsd:float(?tonnage) * 42 *  ?taux , ?tonnage) AS ?tonnage_in_m3)
} 
```

# 5. Introduire des unités de mesure intéropérables

On peut remarquer que deux unités de mesure cohabitent dans cet extrait de navigocorpus pour indiquer le tonnage des navires : 
- tonneaux
- quintaux

Il existe une ontologie pour spécifier les unités de mesure de façon interopérable. La mise en oeuvre dans Portic de cette ontologie permet de questionner tout ce que recouvre la notion de sémantique.

## 5.1. Quantities, Units, Dimensions, and Types

La NASA a construit une ontologie de QUDT [Hodgson et al. 2020] pour capturer et gérer ces informations. QUDT est sous licence CC-BY-4.0 https://creativecommons.org/licenses/by/4.0/.


Les Nations Unies maintiennent une norme appelée UN/Centre for Trade Facilitation and Electronic Business (CEFACT) qui fournit également un ensemble canonique de codes pour identifier sans ambiguïté les unités. QUDT se connecte aux codes UN/CEFACT avec la propriété qudt:uneceCommonCode.

L'ontologie QUDT est bien expliquée sur [son site](http://www.qudt.org).


Les 3 objectifs de QUDT

1. QUDT est un référentiel global pour les unités de mesure internationales : UNITS
2. QUDT fournit des services de conversion entre unités de mesure : QUANTITIES
    Exemple : les tonnages exprimés en quintaux et en tonneaux des historiens. 
3. QUDT permet de vérifier la cohérence des unités attachées à des objets en qualifiant la dimension mesurée: DIMENSIONS
    Exemple : parle-t-on bien toujours de *volume* de navire et de quel volume parle-t-on ?

## 5.2. L'exemple du tonnage d'un navire 

Que mesure-t-on ? La DIMENSION.

Comment mesure-t-on le volume d’un navire, de quoi parle-t-on ? Voir la convention ["International Convention on Tonnage Measurement of Ships, 1969"](https://ec.europa.eu/eurostat/cache/metadata/Annexes/fish_fleet_esms_an1.pdf)  ainsi que cette [thèse](https://commons.wmu.se/cgi/viewcontent.cgi?article=1213&context=all_dissertations). La question n’est pas simple. 

Voir la [méthode MOORSOM](https://fr.wikipedia.org/wiki/M%C3%A9thode_Moorsom), et les articles d’historiens sur la question, pour comprendre la différence entre jauge brute (GRT), et nette (NT). Dans PORTIC, le tonnage correspond à la jauge nette en terme sémantique, car c’est la base pour l’impôt, mais en fait elle est évaluée à vue de nez comme une mesure de la jauge brute car à l’époque, les deux ne sont pas distingués. 



[Gross register tonnage (GRT)](https://worldoceanreview.com/en/wor-1/transport/global-shipping/key-shipping-terms-in-brief/)
> Gross register tonnage or gross tonnage (GT) represents the total internal volume of cargo vessels. 1 GRT = 100 cubic feet ≈ 2.83 cubic metres. Although the term contains the word tonne, the gross register tonnage cannot be equated with measurements of weight such as carrying capacity – nor should it be confused with the standard displacement used to rate a warship, the long ton. Gross register tonnage (GRT) and net register tonnage (NRT) have been replaced by gross tonnage (GT) and net tonnage (NT) which express the size and volume of a ship as a simple dimensionless figure. Port fees and charges for canal passages, locks and pilots are calculated according to the GT or NT.

### 5.2.1. Ce que dit l’historienne Silvia Margalli 

>La question des tonnages est d'une complexité inimaginable. Sous l'ancien régime les unités de mesure peuvent varier d'une province à l'autre juste pro memoria, il a fallu un besoin (le train, au XIXe s.) pour uniformiser l'heure dans le pays (car sinon, il est midi quand le soleil est au zenith, ce qui a le mérite d'être clair, mais pas uniforme…
Du coup la barrique de Bordeaux et de Hambourg ont la même capacité car le pinard de Bordeaux va à Hambourg, mais il y a des barriques dans d'autres provinces qui ont une autre capacité. La littérature n'aide pas beaucoup à mon sens à y voir plus clair.

> Peter se hasarde à fournir des comparaisons intéressantes, mais explique bien que la notion de tonnage est très relative et signifie de choses différentes, calculées différemment, sans que les sources n'indiquent clairement de laquelle elles parlent: *"As McCusker (1997, p. 69) concludes, a British sailing vessel before the American Revolutionary War period “was registered at 100 tons, measured 150 tons, and could carry 200 tons of cargo”.;*

>F. Lane explique que la notion de tonnage couvre 6 types de conceptions et mesures différentes, la française (tonneaux) correspond à la 6e catégorie, les nouvelles normes internationales, implémentées depuis les années 1850 (https://fr.wikipedia.org/wiki/M%C3%A9thode_Moorsom) 



Hypothèse 1 : **1 tonneau = 42 cubic feet**
page 3

> P. Solar et S. Marzagalli, « Tons, Tonneaux, Toneladas, Lasts: British and European Ship Tonnages in the Eighteenth and Early Nineteenth Centuries », Histoire & Mesure, 2021. https://doi.org/10.4000/histoiremesure.13539 
### 5.2.2. Bibliographie

La bible des unités de mesure : Doursther’s Dictionnaire (1840)

D. Allemang et J. Hendler, Semantic Web for the Working Ontologist: Effective Modeling in RDFS and OWL. San Francisco, CA, USA: Morgan Kaufmann Publishers Inc., 2008.

J.-P. Dedieu, S. Marzagalli, P. Pourchasse, et W. Scheltjens, « Navigocorpus at Work: A Brief Overview of the Potential of a Database », International Journal of Maritime History, vol. 24, no 1, p. 331‑360, juin 2012, doi: 10.1177/084387141202400116.

C. J. French, « Eighteenth-Century Shipping Tonnage Measurements », The Journal of Economic History, vol. 33, no 2, p. 434‑443, 1973.

F. C. Lane, « Tonnages, Medieval and Modern », The Economic History Review, vol. 17, no 2, p. 213‑233, 1964, doi: 10.2307/2593003.

P. Solar et S. Marzagalli, « Tons, Tonneaux, Toneladas, Lasts: British and European Ship Tonnages in the Eighteenth and Early Nineteenth Centuries », Histoire & Mesure, 2021. https://doi.org/10.4000/histoiremesure.13539 

G. M. Walton, « Colonial Tonnage Measurements: A Comment », The Journal of Economic History, vol. 27, no 3, p. 392‑397, 1967.



## 5.3. Utiliser QUDT 


http://qudt.org/2.1/vocab/discipline ?

---

Mechanics?
SpaceAndTime? Volume


### 5.3.1. Identifier les unités dans QUDT

Trouver dans http://qudt.org/2.1/vocab/unit . 

tonneaux  : unit de mesure qui n'existe pas dans QUDT
quintaux : non plus

#### 5.3.1.1. cubic meter

```ttl
unit:M3
  a qudt:DerivedUnit ;
  a qudt:Unit ;
  dcterms:description "The SI unit of volume, equal to 1.0e6 cm3, 1000 liters, 35.3147 ft3, or 1.30795 yd3. A cubic meter holds about 264.17 U.S. liquid gallons or 219.99 British Imperial gallons."^^rdf:HTML ;
  qudt:conversionMultiplier 1.0 ;
  qudt:dbpediaMatch "http://dbpedia.org/resource/Cubic_metre"^^xsd:anyURI ;
  qudt:expression "\\(m^{3}\\)"^^qudt:LatexString ;
  qudt:hasDimensionVector qkdv:A0E0L3I0M0H0T0D0 ;
  qudt:hasQuantityKind quantitykind:SectionModulus ;
  qudt:hasQuantityKind quantitykind:Volume ;
  qudt:iec61360Code "0112/2///62720#UAA757" ;
  qudt:informativeReference "http://en.wikipedia.org/wiki/Cubic_metre?oldid=490956678"^^xsd:anyURI ;
  qudt:ucumCode "m3"^^qudt:UCUMcs ;
  qudt:uneceCommonCode "MTQ" ;
  rdfs:isDefinedBy <http://qudt.org/2.1/vocab/unit> ;
  rdfs:label "Cubic Meter"@en-us ;
  rdfs:label "Cubic Metre"@en ;
```
#### 5.3.1.2. cubic feet
```ttl
unit:FT3
  a qudt:DerivedUnit ;
  a qudt:Unit ;
  dcterms:description "The cubic foot is an Imperial and US customary unit of volume, used in the United States and the United Kingdom. It is defined as the volume of a cube with sides of one foot (0.3048 m) in length. To calculate cubic feet multiply length X width X height. "^^rdf:HTML ;
  qudt:conversionMultiplier 0.028316846592 ;
  qudt:definedUnitOfSystem sou:IMPERIAL ;
  qudt:definedUnitOfSystem sou:USCS ;
  qudt:expression "\\(ft^{3}\\)"^^qudt:LatexString ;
  qudt:hasDimensionVector qkdv:A0E0L3I0M0H0T0D0 ;
  qudt:hasQuantityKind quantitykind:Volume ;
  qudt:iec61360Code "0112/2///62720#UAA456" ;
  qudt:ucumCode "[cft_i]"^^qudt:UCUMcs ;
  qudt:ucumCode "[ft_i]3"^^qudt:UCUMcs ;
  qudt:uneceCommonCode "FTQ" ;
  qudt:unitOfSystem sou:IMPERIAL ;
  rdfs:isDefinedBy <http://qudt.org/2.1/vocab/unit> ;
  rdfs:label "Cubic Foot"@en ;
.
```

- Remarquez `qudt:conversionMultiplier 0.028316846592` : 1 cubic feet = 1 * 0.028316846592 m^3
- Remarquez l'hypothèse de conversion : `one foot (0.3048 m) in length`


### 5.3.2. Convertir avec QUDT

Exemple tiré de *Semantic Web for the Working Ontologist* dans son chapitre 14.2 page 393, pour convertir 10 km (unit:KM) en miles (unit:MI)

```ttl
SELECT (((((?val * ?M1) + ?O1) -?O2) / ?M2) AS ?converted)
  WHERE { BIND (10.0 AS ?val) unit:KM qudt:conversionMultiplier ?M1 ;
        qudt:conversionOffset ?O1 .
        unit:MI qudt:conversionMultiplier ?M2 ; 
        qudt:conversionOffset ?O2 . } 
```


> Nous pouvons exprimer ces conversions dans une requête SPARQL, mais cela n'est pas disponible comme inférence. Autrement dit, si nous avons une quantité spécifiée en miles, nous ne pouvons pas toujours compter sur un moteur d'inférence pour exprimer cette valeur en kilomètres. Les calculs de ce genre sont le genre de choses que l'on met habituellement dans un programme d'application. Shapes Constraint Language (SHACL)-Rules (Section 7.1) est un exemple de la façon de faire ces conversions en inférences. Nous avons déjà vu comment SHACL-Rules peut être utilisé pour attacher des requêtes à une ontologie. Ici, nous voyons une autre application des règles SHACL. 
> La requête que nous avons utilisée pour convertir les kilomètres en miles nous a obligés à mettre les informations sur la conversion dans la requête : le nombre à convertir (10,0), l'unité source (kilomètre) et l'unité cible (mile). Si nous voulons faire une autre conversion, nous aurions besoin d'une autre requête, de forme très similaire, mais avec une mesure différente et des désignations d'unités différentes. Bien plus pratique serait de paramétrer la requête, en la transformant efficacement en une définition de fonction. La requête généralisée, utilisant les variables ?arg1, ?arg2, etc., pour les arguments de la fonction, ressemble à :

- arg1 : valeur à transformer (10)
- arg2 : unité de mesure de départ (KM)
- arg3 : unité de mesure à l'arrivée (MI)

```ttl
SELECT (((((?arg1 * ?M1) + ?O1) -?O2) / ?M2) AS ?value)
WHERE {
  ?arg2 qudt:conversionMultiplier ?M1 ;
  qudt:conversionOffset ?O1 .
  ?arg3 qudt:conversionMultiplier ?M2 ;
  qudt:conversionOffset ?O2 .
}
```

> Les extensions SHACL [Steyskal et al. 2017] permettent de donner des noms à de telles requêtes, qui sont elles-mêmes des ressources en RDF. Lorsque nous faisons cela, nous nous référons à la requête nommée en tant que fonction. Si nous donnons à cette requête le nom qudtsh:convert, alors nous pourrions écrire la requête de 10 kilomètres simplement comme

Exercice : 
1) avec SHACL, écrire la fonction qui convertit les tonneaux des navires en mètres cubes, 
2) en SPARQL, écrire la fonction qui récupère toutes les shipsDescription, pour en faire la somme, regroupées par port de départ
  


```ttl
SELECT (qudtsh:convert(tonnage * 42, unit:FT3, unit:M3) AS ?value) WHERE {}
```

Answer: 6.21

Vérification :  http://www.the-converter.net/fr/volumes/cu%20ft/m3

Conclure : 1 tonneau = 1.18 m3
 
 
Mais dans Dedieu et al. page 5 que c'est 1.44 cu meters (1.44 m3) http://navigocorpus.org/iDocuments/Documents/Forum-Dedieu-final.pdf? 

### 5.3.3. Retrouvez la longueur d’un pied français

Parce qu’un pied anglais n’est pas un pied français, et que l’article de Solar et al. fait référence à un pied français (0.324839 m), non l’anglais qu’on trouve dans QUDT (0.028316846592 m3), basé sur la longueur du pied anglais de 0.3048 m).

Par la loi du 19 frimaire an VIII (10 décembre 1799), le mètre définitif fut fixé à « une longueur de 3 pieds 11,296 lignes de la Toise de l'Académie ».

Il faut d'abord retrouver la conversion de toise en mètre, pour savoir que 6 pieds = 1 toise
https://fr.wikipedia.org/wiki/Toise_(unit%C3%A9)#:~:text=La%20toise%20de%20l'Acad%C3%A9mie,pieds%20et%2011%2C38314%20lignes.
1 toise = 1,949 m
6 pied français = 1 toise, 
donc 1 pied français = 0.3248333 m


Il faudrait donc écrire une fonction f(x) de conversion manuelle pour passer des tonneaux aux mètres cubes, se passant du 'cubic feet' de QUDT. 
f(x) = x * 42 * (0.324839)^3 = x * 42 * (0,0342771333938017)

Ou définir une nouvelle unité de mesure FFT3 (French Foot cubic):
```
unit:FFT3
  a qudt:DerivedUnit ;
  a qudt:Unit ;
  dcterms:description "The cubic foot is an Imperial and US customary unit of volume, used in the United States and the United Kingdom. It is defined as the volume of a cube with sides of one foot (0.3048 m) in length. To calculate cubic feet multiply length X width X height. "^^rdf:HTML ;
  qudt:conversionMultiplier 0.0342771333938017 ;
  qudt:definedUnitOfSystem sou:IMPERIAL ;
  qudt:definedUnitOfSystem sou:USCS ;
  qudt:expression "\\(ft^{3}\\)"^^qudt:LatexString ;
  qudt:hasDimensionVector qkdv:A0E0L3I0M0H0T0D0 ;
  qudt:hasQuantityKind quantitykind:Volume ;
  qudt:iec61360Code "0112/2///62720#UAA456" ;
  qudt:ucumCode "[cft_i]"^^qudt:UCUMcs ;
  qudt:ucumCode "[ft_i]3"^^qudt:UCUMcs ;
  qudt:uneceCommonCode "FTQ" ;
  qudt:unitOfSystem sou:IMPERIAL ;
  rdfs:isDefinedBy <http://qudt.org/2.1/vocab/unit> ;
  rdfs:label "Cubic Foot"@en ;
.
```



- $(tonnage) : valeur
- $(tonnage_unit) : unité qui peut être tonneaux ou quintaux

Exercice
**Faire un sparql update pour spécifier unit en M3 le volumes des navires**



Et maintenant comment convertir les quintaux de Marseille en tonneaux : il faut diviser par 24. 
1 tonneau = 24 quintaux, donc les valeurs de marseille doivent être converties en tonneaux en divisant par 24


Exercice

# 6. Vérifier la consistence des données avec SHACL

From : https://www.linkedin.com/pulse/meet-shacl-next-owl-kurt-cagle/
> OWL works by essentially constraining information coming into a triple store; most OWL stores will not allow you to save triples to the store if it violates a specific restriction set by the OWL language. SHACL, on the other hand, works by using SPARQL to validate specific content that may already be saved to the triple store. This is a subtle but important distinction: content can be stored within a triple store even if the data is not necessarily structurally valid (a situation which occurs quite often when pulling data from external data sources).


[SHACL](https://www.w3.org/TR/shacl/) : W3C standard 2017

# 7. Suite 

Pour poursuivre, inscrivez vous sur la [liste de diffusion](https://listes.services.cnrs.fr/wws/info/ateliers-sparql) 